// Problem 6:
// Write a method called evenlyDivisible that accepts two integer parameters and 
// returns true if the first parameter is evenly divisible by the second, or 
// vice versa, and false otherwise. Return false if either parameter is zero.

public class Driver
{

  public static void main(String[] args)
  {
    Utility util = new Utility();
    int iValue = 15;
    int iNumber = 60;
    
    if (util.evenlyDivisible(iValue, iNumber))
    {
      System.out.println("The two numbers: " + iValue + " and " + iNumber
                         + " are divisible");
    }
    else
    {
      System.out.println("Not divisible");
    }
    
  }

}
