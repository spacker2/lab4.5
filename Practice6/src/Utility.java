import java.util.*;

public class Utility
{

  public ArrayList<Integer> generate50RandInts()
  {
    final int MAX = 3;
    Random gen = new Random();
    ArrayList<Integer> list = new ArrayList<Integer>();
    int counter = 1;
    
    while (counter <= MAX)
    {
      int myRandom = gen.nextInt();
      list.add(myRandom);
      
      counter++;
    }
    return list;
  }
}
