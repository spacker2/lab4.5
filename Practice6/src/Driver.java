// Practice 6:
// ---------
// Write a java application that randomly-generates an ArrayList of 50 integer 
// values (use a loop to do this). 

import java.util.*;

public class Driver
{

  public static void main(String[] hi)
  {
    Utility util = new Utility();
    ArrayList<Integer> myList = new ArrayList<Integer>();
    
    myList = util.generate50RandInts();
    System.out.println(myList);

  }

}
