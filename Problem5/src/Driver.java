import java.util.*;
import java.io.*;


// Design and implement a program that compares two text input files, line by 
// line, for equality. Print any lines that are not equivalent.



public class Driver
{

  public static void main(String[] args) throws IOException
  {
    Scanner fileScan1 = new Scanner(new File("data1.txt"));
    Scanner fileScan2 = new Scanner(new File("data2.txt"));

    while (fileScan1.hasNextLine() && fileScan2.hasNextLine())
    {
      String line1 = fileScan1.nextLine();
      String line2 = fileScan2.nextLine();
      
//      if (line1.equals(line2))
      if (line1.compareTo(line2) == 0)
      {
        System.out.println("Both files contain the same line : " + line1);
      }
      else
      {
        System.out.println("File1 contains line : " + line1);
        System.out.println("File2 contains line : " + line2);
      }
    }
  }

}
