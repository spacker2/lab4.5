// Practice 2:
// ---------
// Write a segment of Java code that will allow the user to enter an arbitrary 
// series of negative numbers between -500 and -50.  If the user enters number 
// outside the specified range, the code should display product of all numbers 
// entered.


import java.util.*;


public class Driver
{

  public static void main(String[] args)
  {
    final int MIN = -500;
    final int MAX = -50;
    int iProduct = 1;
    int iValue = 0;
    Scanner scan = new Scanner(System.in);

    System.out
        .println("Enter int values between " + MIN + " and " + MAX + ": ");
    iValue = scan.nextInt();

    while ((MIN <= iValue) && (iValue <= MAX))
    {
      iProduct = iProduct * iValue;

      System.out
          .println("Enter int values between " + MIN + " and " + MAX + ": ");
      iValue = scan.nextInt();
    }
    System.out.println("Product of all entered numbers is " + iProduct);

  }

}
