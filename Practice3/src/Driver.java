// Practice 3:
// ---------
// Write a method that receives a String parameter, and returns the String 
// reversed and in lower case.

public class Driver
{

  public static void main(String[] args)
  {
    Utility util = new Utility();
    
    String str = "aBcD1 ; Hi NoW";
    
    str = util.reverseAndLower(str);
    System.out.println(str);

  }

}
