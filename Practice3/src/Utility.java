
public class Utility
{
  
  public String reverseAndLower(String myString)
  {
    String strLower = myString.toLowerCase();
    String strResult = "";
    int position =  myString.length() - 1;
    
    while (position >= 0)
    {
      char myChar = strLower.charAt(position);
      strResult = strResult + myChar;
      position--;
    }
    
    return strResult;
    
  }
}
