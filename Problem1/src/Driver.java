
// Problem 1:
// Write a code fragment that reads and prints integer values entered by a user 
// until a particular sentinel value (stored in SENTINEL) is entered. Do not 
// print the sentinel value.

import java.util.*;

public class Driver
{

  public static void main(String[] args)
  {

    Scanner scanner = new Scanner(System.in);
    int iValue = 0;
    final int SENTINEL = -1;

    System.out.println("Enter an integer or " + SENTINEL + " to teminate");
    iValue = scanner.nextInt();

    while (iValue != SENTINEL)
    {
      System.out.println("You Entered " + iValue);

      System.out.println("Enter an integer or " + SENTINEL + "to teminate");
      iValue = scanner.nextInt();
    }

    System.out.println("Thanks");
  }

}
