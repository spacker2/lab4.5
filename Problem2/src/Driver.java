// Problem 2:
// Write a method called isAlpha that accepts a character parameter and returns 
// true if that character is either an uppercase or lowercase alphabetic letter.

public class Driver
{
  
  public static void main(String[] args)
  {
    char myChar ='t';
    
    Utility util = new Utility();
    
    if (util.isAlpha(myChar))
    {
      System.out.println(myChar + " is alphabetic character");
    }
    
    myChar = ';';
    
    if (util.isAlpha(myChar))
    {
      System.out.println(myChar + " is alphabetic character");
    }
    else
    {
      System.out.println(myChar + " is NOT alphabetic character");
    }

  }

}
